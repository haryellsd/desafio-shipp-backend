<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DBImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:db {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa lojas de um .csv para o banco.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = $this->argument('file');
        $fileParts = pathinfo($file);

        if (strtolower($fileParts['extension']) !== 'csv') {
            return $this->warn('O arquivo deve ser no formato .csv.');
        }

        if (!file_exists($file)) {
            return $this->warn('Arquivo não existe.');
        }

        $hasHeader = $this->confirm('Esse arquivo possui cabeçalho? [yes|no]');

        return $this->processFile($file, $hasHeader);
    }

    protected function processFile($file, $hasHeader)
    {
        $this->comment('Processando arquivo.');

        try {
            $inserted = 1;
            DB::beginTransaction();
            if (($handle = fopen($file, 'r')) !== false) {
                $row = 0;
                while (($data = fgetcsv($handle, 600, ',')) !== false) {
                    if ($row === 0 && $hasHeader) {
                        $row++;
                        continue;
                    }

                    $locationStr = strtolower(str_replace(['"', "'"], ['', '"'], trim($data[14])));
                    $location = json_decode($locationStr);

                    if (isset($location->latitude) && isset($location->longitude)) {
                        $latRad = $location->latitude * pi() / 180;
                        $longRad = $location->longitude * pi() / 180;
                    }

                    DB::table('stores')->insert([
                        [
                            'county' => trim($data[0]),
                            'license_number' => trim($data[1]),
                            'operation_type' => trim($data[2]),
                            'establishment_type' => trim($data[3]),
                            'entity_name' => trim($data[4]),
                            'dba_name' => trim($data[5]),
                            'street_number' => trim($data[6]),
                            'street_name' => trim($data[7]),
                            'address_line_2' => trim($data[8]),
                            'address_line_3' => trim($data[9]),
                            'city' => trim($data[10]),
                            'state' => trim($data[11]),
                            'zip_code' => trim($data[12]),
                            'square_footage' => trim($data[13]),
                            'location' => trim($data[14]),
                            'sin_lat' => isset($location->latitude) ? sin($latRad) : null,
                            'cos_lat' => isset($location->latitude) ? cos($latRad) : null,
                            'sin_long' => isset($location->latitude) ? sin($longRad) : null,
                            'cos_long' => isset($location->latitude) ? cos($longRad) : null
                        ]
                    ]);
                    $inserted++;
                    $row ++;
                }
                DB::commit();
                return $this->info("Arquivo processado com sucesso. $inserted registros inseridos.");
            } else {
                return $this->warn('Falha ao ler arquivo.');
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            var_dump($ex->getMessage());
            return $this->warn('Falha ao importar o arquivo.');
        }
    }
}
