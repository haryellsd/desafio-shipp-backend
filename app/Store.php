<?php

namespace App;

use App\Http\Resources\StoreResource;
use function foo\func;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Store extends Model
{
    protected $guarded = [];

    static $maxDist = 6.5;

    public static function findClose($latitude, $longitude) {
        $latRad = $latitude * pi() / 180;
        $longRad = $longitude * pi() / 180;

        $locations = DB::table('stores')
            ->selectRaw('*, (? * sin_lat + ? * cos_lat * (? * sin_long + ? * cos_long)) as distance_acos',
                [sin($latRad), cos($latRad), sin($longRad), cos($longRad)])
            ->where('lat_rad', '<>', null)
            ->where('cos_rad', '<>', null)
            ->orderBy('distance_acos', 'desc')
            ->get()
            ->reject(function($local) {
                $dist = Store::calcDist($local->distance_acos);
                if ($dist > Store::$maxDist) {
                    return $local;
                }
            });

        return StoreResource::collection($locations);
    }

    public static function calcDist($distAcos) {
        return (!is_nan(acos($distAcos))) ? acos($distAcos) * 6371 : 0;
    }
}
