<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class LogRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * @var JsonResponse $response
         */
        $response = $next($request);
        $latitude = $request->query('latitude');
        $longitude = $request->query('longitude');

        DB::table('log_requests')->insert([
            [
                'latitude' => $latitude,
                'longitude' => $longitude,
                'status_code' => $response->getStatusCode(),
                'stores_count' => ($response->getStatusCode() === 200) ? count($response->getData()) : null
            ]
        ]);

        return $response;
    }
}
