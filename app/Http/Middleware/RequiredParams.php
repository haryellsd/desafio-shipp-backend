<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;

class RequiredParams
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->has('longitude')) {
            return new JsonResponse(['message' => 'Parâmetro [longitude] é obrigatório.'], 400);
        }

        if (!is_numeric($request->query('longitude'))) {
            return new JsonResponse(['message' => 'Parâmetro [longitude] deve ser numérico.'], 400);
        }

        if (!$request->has('latitude')) {
            return new JsonResponse(['message'=> 'Parâmetro [latitude] é obrigatório.'], 400);
        }

        if (!is_numeric($request->query('latitude')))
        {
            return new JsonResponse(['message' => 'Parâmetro [latitude] deve ser numérico.'], 400);
        }

        return $next($request);
    }
}
