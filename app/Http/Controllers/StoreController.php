<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use function MongoDB\BSON\toJSON;
use phpDocumentor\Reflection\Types\Resource_;
use Symfony\Component\HttpFoundation\Tests\JsonSerializableObject;

class StoreController extends Controller
{
    /**
     * Retorna uma lista de locais próximos a latitude e longitude informadas.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $longitude = request()->query('longitude');
        $latitude = request()->query('latitude');

        $stores = Store::findClose($latitude, $longitude);

        return response()->json($stores);
    }
}
