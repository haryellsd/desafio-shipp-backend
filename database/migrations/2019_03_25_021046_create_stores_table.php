<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id_store');
            $table->unique('id_store');
            $table->string('county');
            $table->integer('license_number');
            $table->string('operation_type');
            $table->string('establishment_type');
            $table->string('entity_name');
            $table->string('dba_name');
            $table->integer('street_number');
            $table->string('street_name');
            $table->string('address_line_2');
            $table->string('address_line_3');
            $table->string('city');
            $table->string('state');
            $table->integer('zip_code');
            $table->integer('square_footage');
            $table->json('location');
            $table->float('sin_lat')->nullable(true);
            $table->float('cos_lat')->nullable(true);
            $table->float('sin_long')->nullable(true);
            $table->float('cos_long')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
