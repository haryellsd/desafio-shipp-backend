<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class stores extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/V1/stores')->assertStatus(400);
        $this->get('/V1/stores?longitude=20&latitude=20')->assertStatus(200);
    }
}
